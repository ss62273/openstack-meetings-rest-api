package assign.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 3 REST API (GETs) and Functional Testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class TestMyEavesdropResource {

  private static boolean allTestPassed = true;

  // Custom Response Handler
  public static class ResponseHandlerImpl extends Object implements ResponseHandler<String> {

    public ResponseHandlerImpl() {

    }

    public String handleResponse(final HttpResponse response)
        throws ClientProtocolException, IOException {
      int status = response.getStatusLine().getStatusCode();
      if (status >= 200 && status < 300) {
        HttpEntity entity = response.getEntity();
        return entity != null ? EntityUtils.toString(entity) : null;
      } else {
        throw new ClientProtocolException("Unexpected response status: " + status);
      }
    }
  }

  // Tests whether 10 projects exist in the response
  public static void testGetAllProjects(CloseableHttpClient httpclient, HttpGet httpget,
      ResponseHandlerImpl responseHandler) throws ClientProtocolException, IOException {
    String[] expectedOutput = {"3rd_party_ci/", "compass_dev/", "gantt/", "marconi_team/",
        "neutronlbaas/", "openstack-freezer/", "openstack-product/", "solum_team_meeting/",
        "user_committee/", "zun/"};
    List<String> resultOutput = new ArrayList<String>();

    System.out.println("Executing request " + httpget.getRequestLine());

    String responseBody = httpclient.execute(httpget, responseHandler);

    Document document = Jsoup.parse(responseBody, "", Parser.xmlParser());
    for (Element e : document.select("project")) {
      resultOutput.add(e.html());
    }
    try {
      for (String s : Arrays.asList(expectedOutput)) {
        assertTrue("testGetAllProjects failed.", resultOutput.contains(s));
      }
    } catch (AssertionError e) {
      allTestPassed = false;
      e.printStackTrace();
    }
  }

  // Tests all the years in the response are correct
  public static void testGetAllMeetings(CloseableHttpClient httpclient, HttpGet httpget,
      ResponseHandlerImpl responseHandler) throws ClientProtocolException, IOException {
    String[] expectedOutput = {"2013/", "2014/", "2015/", "2016/", "2017/"};
    List<String> resultOutput = new ArrayList<String>();

    System.out.println("Executing request " + httpget.getRequestLine());

    String responseBody = httpclient.execute(httpget, responseHandler);

    Document document = Jsoup.parse(responseBody, "", Parser.xmlParser());
    for (Element e : document.select("year")) {
      resultOutput.add(e.html());
    }
    try {
      assertEquals("testGetAllMeetings failed.", resultOutput, Arrays.asList(expectedOutput));
    } catch (AssertionError e) {
      allTestPassed = false;
      e.printStackTrace();
    }
  }

  // Tests correct XML format in the response
  public static void testXMLFormat(CloseableHttpClient httpclient, HttpGet httpget,
      ResponseHandlerImpl responseHandler) throws ClientProtocolException, IOException {
    String expectedOutput = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
        + "<meetings>\n" + "    <year>2014/</year>\n" + "</meetings>\n";

    System.out.println("Executing request " + httpget.getRequestLine());

    String responseBody = httpclient.execute(httpget, responseHandler);

    try {
      assertEquals("testXMLFormat failed.", expectedOutput, responseBody);
    } catch (AssertionError e) {
      allTestPassed = false;
      e.printStackTrace();
    }
  }

  // Tests non-existent project response
  public static void testNonExistentProject(CloseableHttpClient httpclient, HttpGet httpget,
      ResponseHandlerImpl responseHandler) throws ClientProtocolException, IOException {
    String expectedOutput = "Project non-existent-project does not exist";

    System.out.println("Executing request " + httpget.getRequestLine());

    String responseBody = httpclient.execute(httpget, responseHandler);

    Document document = Jsoup.parse(responseBody, "", Parser.xmlParser());
    String resultOutput = document.select("error").html();

    try {
      assertEquals("testNonExistentProject failed.", expectedOutput, resultOutput);
    } catch (AssertionError e) {
      allTestPassed = false;
      e.printStackTrace();
    }
  }

  // main
  public final static void main(String[] args) throws IOException {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    ResponseHandlerImpl responseHandler = new ResponseHandlerImpl();
    try {
      HttpGet httpget = new HttpGet("http://localhost:8080/assignment3/myeavesdrop/projects/");
      testGetAllProjects(httpclient, httpget, responseHandler);

      httpget = new HttpGet(
          "http://localhost:8080/assignment3/myeavesdrop/projects/solum_team_meeting/meetings");
      testGetAllMeetings(httpclient, httpget, responseHandler);

      httpget = new HttpGet(
          "http://localhost:8080/assignment3/myeavesdrop/projects/3rd_party_ci/meetings");
      testXMLFormat(httpclient, httpget, responseHandler);

      httpget = new HttpGet(
          "http://localhost:8080/assignment3/myeavesdrop/projects/non-existent-project/meetings");
      testNonExistentProject(httpclient, httpget, responseHandler);

      System.out.println("----------------------------------------");
      if (allTestPassed) {
        System.out.println("All tests passed.");
      } else {
        System.out.println("Caught Assertion Error.");
      }
    } finally {
      httpclient.close();
    }
  }

}
