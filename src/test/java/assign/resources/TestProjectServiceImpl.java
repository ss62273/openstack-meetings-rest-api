package assign.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import assign.domain.NewProject;
import assign.services.ProjectService;
import assign.services.ProjectServiceImpl;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 4 REST API � POST, PUT, DELETE
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class TestProjectServiceImpl {

  ProjectService projectService = null;
  Logger testLogger = Logger.getLogger("testlogger");

  @Before
  public void setUp() {
    final String dbhost = "localhost";
    final String dbname = "sangjinshin_cs378_db";
    final String dbusername = "root";
    final String dbpassword = "";
    projectService = new ProjectServiceImpl(dbhost, dbname, dbusername, dbpassword);
  }

  @Test
  public void testProjectAddition() {
    try {
      NewProject p = new NewProject();
      p.setName("Test Project");
      p.setDescription("Test Description");
      p = projectService.addProject(p);

      NewProject p1 = projectService.getProject(p.getProjectId());

      assertEquals(p1.getName(), p.getName());
      assertEquals(p1.getDescription(), p.getDescription());
      assertEquals(p1.getProjectId(), p.getProjectId());

      projectService.deleteProject(p.getProjectId());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testProjectGet() {
    try {
      NewProject p = new NewProject();
      p.setName("Test Project");
      p.setDescription("Test Description");
      p = projectService.addProject(p);

      NewProject p1 = projectService.getProject(p.getProjectId());
      testLogger.logp(Level.INFO, "TestProjectServiceImpl", "testProjectGet",
          p.getName() + ", " + p.getDescription() + ", " + String.valueOf(p.getProjectId()));

      assertEquals(p1.getName(), p.getName());
      assertEquals(p1.getDescription(), p.getDescription());
      assertEquals(p1.getProjectId(), p.getProjectId());

      projectService.deleteProject(p.getProjectId());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testProjectUpdate() {
    try {
      NewProject p = new NewProject();
      p.setName("Test Project");
      p.setDescription("Test Description");
      p = projectService.addProject(p);

      NewProject p1 = new NewProject();
      p1.setName("Updated Project");
      p1.setDescription("Updated Description");
      p1.setProjectId(p.getProjectId());

      try {
        boolean isSuccessful = projectService.updateProject(p1.getProjectId(), p1);
        assertNotEquals(p1.getName(), p.getName());
        assertNotEquals(p1.getDescription(), p.getDescription());
        testLogger.logp(Level.INFO, "TestProjectServiceImpl", "testProjectUpdate",
            p.getName() + ", " + p.getDescription() + ", " + String.valueOf(p.getProjectId()));
        if (isSuccessful) {
          testLogger.logp(Level.INFO, "TestProjectServiceImpl", "testProjectUpdate",
              p1.getName() + ", " + p1.getDescription() + ", " + String.valueOf(p1.getProjectId()));

          projectService.deleteProject(p.getProjectId());
        }
      } catch (AssertionError e) {
        testLogger.logp(Level.WARNING, "TestProjectServiceImpl", "testProjectUpdate",
            p1.getName() + ", " + p1.getDescription() + ", " + String.valueOf(p1.getProjectId()));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testProjectDelete() {
    try {
      NewProject p = new NewProject();
      p.setName("Test Project");
      p.setDescription("Test Description");
      p = projectService.addProject(p);

      int project_id = p.getProjectId();
      assertTrue(projectService.deleteProject(p.getProjectId()));
      assertTrue(projectService.getProject(project_id) == null);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
