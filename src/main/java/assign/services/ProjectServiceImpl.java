package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import assign.domain.NewProject;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 4 REST API � POST, PUT, DELETE
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class ProjectServiceImpl implements ProjectService {

  String dbHost = "";
  String dbPortNumber = "";
  String dbName = "";
  String dbUsername = "";
  String dbPassword = "";
  DataSource ds;

  public ProjectServiceImpl(String dbHost, String dbName, String username, String password) {
    this.dbHost = dbHost;
    this.dbPortNumber = "3306";
    this.dbName = dbName;
    this.dbUsername = username;
    this.dbPassword = password;

    ds = setupDataSource();
  }

  public DataSource setupDataSource() {
    BasicDataSource ds = new BasicDataSource();
    ds.setUsername(this.dbUsername);
    ds.setPassword(this.dbPassword);
    ds.setUrl("jdbc:mysql://" + this.dbHost + ":" + this.dbPortNumber + "/" + this.dbName);
    ds.setDriverClassName("com.mysql.jdbc.Driver");
    return ds;
  }

  @Override
  public NewProject getProject(int project_id) throws Exception {
    String query = "SELECT * from projects where project_id = ?";

    Connection conn = ds.getConnection();

    PreparedStatement s = conn.prepareStatement(query);
    s.setString(1, String.valueOf(project_id));

    ResultSet r = s.executeQuery();

    if (!r.next()) {
      return null;
    }

    NewProject project = new NewProject();
    project.setName(r.getString("name"));
    project.setDescription(r.getString("description"));
    project.setProjectId(r.getInt("project_id"));

    return project;
  }

  @Override
  public NewProject addProject(NewProject project) throws Exception {
    String insert = "INSERT INTO projects(name, description) VALUES(?, ?)";

    Connection conn = ds.getConnection();

    PreparedStatement stmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
    stmt.setString(1, project.getName());
    stmt.setString(2, project.getDescription());

    int affectedRows = stmt.executeUpdate();

    if (affectedRows == 0) {
      throw new SQLException("Creating project failed, no rows affected.");
    }

    ResultSet generatedKeys = stmt.getGeneratedKeys();
    if (generatedKeys.next()) {
      project.setProjectId(generatedKeys.getInt(1));
    } else {
      throw new SQLException("Creating project failed, no ID obtained.");
    }

    // Close the connection
    conn.close();

    return project;
  }

  @Override
  public boolean updateProject(int projectId, NewProject project) throws Exception {
    boolean isSuccessful = false;
    String query = "UPDATE projects SET name = ?, description = ? WHERE project_id = ?";

    Connection conn = ds.getConnection();

    PreparedStatement stmt = conn.prepareStatement(query);
    stmt.setString(1, project.getName());
    stmt.setString(2, project.getDescription());
    stmt.setString(3, String.valueOf(projectId));

    int affectedRows = stmt.executeUpdate();

    if (affectedRows > 0) {
      isSuccessful = true;
    }

    // Close the connection
    conn.close();

    return isSuccessful;
  }

  @Override
  public boolean deleteProject(int projectId) throws Exception {
    boolean isSuccessful = false;
    String query = "DELETE FROM projects WHERE project_id = ?";

    Connection conn = ds.getConnection();

    PreparedStatement stmt = conn.prepareStatement(query);
    stmt.setString(1, String.valueOf(projectId));

    int affectedRows = stmt.executeUpdate();

    if (affectedRows > 0) {
      isSuccessful = true;
    }

    // Close the connection
    conn.close();

    return isSuccessful;
  }
}
