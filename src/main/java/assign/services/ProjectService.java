package assign.services;

import assign.domain.NewProject;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 4 REST API � POST, PUT, DELETE
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public interface ProjectService {

  public NewProject getProject(int project_id) throws Exception;

  public NewProject addProject(NewProject project) throws Exception;

  public boolean updateProject(int project_id, NewProject project) throws Exception;

  public boolean deleteProject(int project_id) throws Exception;

}
