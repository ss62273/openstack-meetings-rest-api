package assign.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 4 REST API � POST, PUT, DELETE
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class NewProject {

  String name;
  String description;
  @XmlAttribute
  int project_id;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getProjectId() {
    return project_id;
  }

  public void setProjectId(int project_id) {
    this.project_id = project_id;
  }
}
