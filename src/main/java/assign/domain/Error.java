package assign.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 3 REST API (GETs) and Functional Testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@XmlRootElement(name = "output")
@XmlAccessorType(XmlAccessType.FIELD)
public class Error {

  String error = null;

  public String getError() {
    return this.error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
