package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import assign.domain.Error;
import assign.domain.Meetings;
import assign.domain.NewProject;
import assign.domain.Projects;
import assign.services.EavesdropService;
import assign.services.ProjectService;
import assign.services.ProjectServiceImpl;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * RESTEasy Eavesdrop Application
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Path("/projects")
public class MyEavesdropResource {

  EavesdropService eavesdropService;
  ProjectService projectService;
  String dbhost;
  String dbname;
  String password;
  String username;

  public MyEavesdropResource(@Context ServletContext servletContext) {
    this.eavesdropService = new EavesdropService();
    dbhost = servletContext.getInitParameter("DBHOST");
    dbname = servletContext.getInitParameter("DBNAME");
    username = servletContext.getInitParameter("DBUSERNAME");
    password = servletContext.getInitParameter("DBPASSWORD");
    this.projectService = new ProjectServiceImpl(dbhost, dbname, username, password);
  }

  @GET
  @Path("/")
  @Produces("application/xml")
  public StreamingOutput getAllProjects() throws Exception {

    final Projects projects = new Projects();
    projects.setProjects(this.eavesdropService.getAllProjects());

    return new StreamingOutput() {
      public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        outputProjects(outputStream, projects);
      }
    };
  }

  @POST
  @Path("/")
  @Consumes("application/xml")
  public Response createProject(NewProject project) throws Exception {

    // Return 400 Bad Request: invalid project parameter
    if (project == null || project.getName() == null || project.getName().length() < 1
        || project.getDescription() == null || project.getDescription().length() < 1) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    final NewProject newProject = projectService.addProject(project);
    final URI path = URI.create("/projects/" + newProject.getProjectId());

    // 201 Created
    return Response.created(path).build();
  }

  @PUT
  @Path("/{project_id}")
  @Consumes("application/xml")
  public Response updateProject(@PathParam("project_id") Integer project_id,
      NewProject updateProject) throws Exception {

    // Return 400 Bad Request: invalid parameters are passed in
    if (updateProject == null || updateProject.getName() == null
        || updateProject.getName().length() < 1 || updateProject.getDescription() == null
        || updateProject.getDescription().length() < 1) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    NewProject currentProject = projectService.getProject(project_id);
    // Return 404 Not Found: project with project_id does not exist
    if (currentProject == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    // Update
    boolean isSuccessful = projectService.updateProject(project_id, updateProject);

    if (isSuccessful) {
      // Return 204 No Content: Update Successful
      return Response.status(Response.Status.NO_CONTENT).build();
    } else {
      // Should not reach here
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  @GET
  @Path("/{project_id}")
  @Produces("application/xml")
  public Response getProject(@PathParam("project_id") Integer project_id) throws Exception {

    // Return 404 Not Found: invalid project_id is passed in
    if (project_id == null || project_id < 0) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    final NewProject project = projectService.getProject(project_id);
    // Return 404 Not Found: project with provided project id does not exist
    if (project == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    StreamingOutput stream = new StreamingOutput() {
      public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        outputProject(outputStream, project);
      }
    };

    // 200 OK: Request successful
    return Response.ok(stream).build();
  }

  @DELETE
  @Path("/{project_id}")
  @Produces("application/xml")
  public Response deleteProject(@PathParam("project_id") Integer project_id) throws Exception {

    // Return 404 Not Found: invalid project_id is passed in
    if (project_id == null || project_id < 0) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    NewProject project = projectService.getProject(project_id);
    // Return 404 Not Found: project with project_id does not exist
    if (project == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    boolean isSuccessful = projectService.deleteProject(project_id);

    if (!isSuccessful) {
      // Return 404 Not Found: There is no project with project_id to delete
      return Response.status(Response.Status.NOT_FOUND).build();
    } else {
      // Return 200 OK: Delete successful
      return Response.ok().build();
    }
  }

  @GET
  @Path("/{project}/meetings")
  @Produces("application/xml")
  public StreamingOutput getAllMeetings(@PathParam("project") final String project)
      throws Exception {

    // Make project parameter case-sensitive
    if (!project.toLowerCase().equals(project)) {
      return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
          final Error error = new Error();
          error.setError("Project " + project + " does not exist");
          outputError(outputStream, error);
        }
      };
    }

    final Meetings meetings = new Meetings();
    meetings.setMeetings(this.eavesdropService.getAllMeetings(project));

    return new StreamingOutput() {

      public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        if (meetings.getMeetings() == null || meetings.getMeetings().isEmpty()) {
          final Error error = new Error();
          error.setError("Project " + project + " does not exist");
          outputError(outputStream, error);
        } else
          outputMeetings(outputStream, meetings);
      }

    };
  }

  protected void outputProjects(OutputStream os, Projects projects) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(projects, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }

  protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(meetings, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }

  protected void outputError(OutputStream os, Error error) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Error.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(error, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }

  protected void outputProject(OutputStream os, NewProject project) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(NewProject.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(project, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }
}
